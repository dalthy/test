#-------------------------------------------------
#
# Project created by QtCreator 2012-10-26T20:03:10
#
#-------------------------------------------------

QT       += core gui

TARGET = hello
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
